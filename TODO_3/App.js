
import { StyleSheet, Text, View } from 'react-native';
import {person,file} from './components/namedExport';
import anyname from './components/defaultExport';
export default function App() {
  return (
    <View style={styles.container}>
      <Text>name: {person()}</Text>
      <Text>file1: {file()}</Text>
      <Text>file2: {anyname()}</Text>

     
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
