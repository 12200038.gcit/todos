import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
export default function App() {
  const name="BIR BDR TAMANG"
  return (
    <View style={styles.container}>
      <Text style={styles.text}>Getting started with React Native!</Text>
      <Text style={styles.myname}>My name is {name}</Text>

      <StatusBar style="auto" />

    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text:{
    fontSize:30
  },
  myname:{
    fontSize:20
  },
});
