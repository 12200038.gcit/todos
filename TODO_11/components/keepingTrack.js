import { StyleSheet, Text, View,ToastAndroid } from 'react-native'
import React, { useState } from 'react'
import { Button} from 'react-native'

const keepingTrack = () => {
    const [count,setCount]=useState(0)
    const [disable,setDisable]=useState(false)
    function func(){
      setCount(count+1)
      if(count>=2){
        setDisable(true)
        ToastAndroid.showWithGravityAndOffset(
          "Sorry, the button is disabled",
          ToastAndroid.LONG,
          ToastAndroid.BOTTOM,
          25,
          50
        );
      }
    }
  return (
    <View>
      <Text>You clicked {count} times</Text>
      <Button
        title='Press Me'
        disabled={disable}
        onPress={func}
      />
    </View>
  )
}
export default keepingTrack

const styles = StyleSheet.create({})