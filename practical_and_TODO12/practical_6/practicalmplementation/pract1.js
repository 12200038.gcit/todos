import { StyleSheet, Text, View,TextInput,Button,ScrollView,FlatList } from 'react-native'
import React, { useState } from 'react'

const pract1 = () => {
    const [enteredAspiration,setEnterAspiration]=useState("")
    const [courseAspirations,setCourseAspirations]=useState([])
    const AspirationInputHandler=(enteredText)=>{
        setEnterAspiration(enteredText)
    }

    const addAspirationHandler=()=>{
        setCourseAspirations(currentAspirations=>[...currentAspirations,{key:Math.random().toString(),value:enteredAspiration}])
        setEnterAspiration("")
    }
  return (
    <View style={styles.screen}>
        <View style={styles.inputContainer}>
            <TextInput
                placeholder='My Aspiration from this module'
                style={styles.input}
                value={enteredAspiration}
                onChangeText={AspirationInputHandler}
            />
            <Button
                title='ADD'
                onPress={addAspirationHandler}
            />
        </View>
        <FlatList
            data={courseAspirations}
            renderItem={({item})=>(
                <View style={styles.listItems}>
                    <Text>{item.value}</Text>
                </View>
            )}
            // renderItem={itemData=>{
            //     <View style={styles.listItems}>
            //         <Text>{itemData.item.value}</Text>
            //     </View>
            // }}
        />
     
    </View>
  )
}

export default pract1

const styles = StyleSheet.create({
    screen:{
        padding:50
    },
    inputContainer:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
    },
    input:{
        width:'80%',
        borderColor:'black',
        borderWidth:1,
        padding:10
    },
    listItems:{
        padding:10,
        marginVertical:10,
        backgroundColor:'grey',
        borderColor:'black',
        borderWidth:1
    }
})