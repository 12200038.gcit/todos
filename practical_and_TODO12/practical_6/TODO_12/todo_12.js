import { StyleSheet, Text,Button,View,TextInput,Pressable } from 'react-native'
import React, { useState } from 'react'

const todo_12 = () => {
    const [text,setText]=useState("")
    const [newText,setNewText]=useState([])

    const change=(props)=>{
        setText(props)
    }
    const addText=()=>{
        setNewText([...newText,text])
    }
    const eraseText=()=>{
        setText("")
    }
  return (
    <View style={styles.container}>
        <View>
            {newText.map(output=><Text>{output}</Text>)}
        </View>
      <TextInput
        placeholder='Enter text here'
        style={styles.input}
        onChangeText={change}
        value={text}

      />
      <View style={styles.buttons}>

        <Pressable style={styles.button} onPress={eraseText} >
          <Text style={styles.cancel}>CANCEL</Text>
        </Pressable>

        <Pressable style={styles.button}>
          <Text style={styles.add} onPress={addText}>ADD</Text>
        </Pressable>

      </View>
     
    </View>
  )
}

export default todo_12

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    },
    buttons:{
        flexDirection:'row'

    },
    button:{
        marginHorizontal:20
    },
    cancel:{
        color:'red',
        fontWeight:'bold'
    },
    add:{
        color:'blue',
        fontWeight:'bold'
    },
    input:{
        borderWidth:1,
        width:300,
        height:40,
        marginVertical:10,
        paddingHorizontal:5
    }
})