import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
// import Pract1 from './practicalmplementation/pract1';
import Todo_12 from './TODO_12/todo_12';
export default function App() {
  return (
    <View style={styles.container}>
      {/* // <Pract1/> */}
      <Todo_12/>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
});
