import 'react-native-gesture-handler';
import { StyleSheet, Text, View,Image,Button } from 'react-native';
import { NavigationContainer, TabActions } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
// import Screen1 from './Todo13/Screen1';
// import Screen2 from './Todo13/Screen2';
// import Screen3 from './Todo13/Screen3';
import {Provider } from 'react-native-paper';
import { theme } from './src/core/theme';
import {
  StartScreen,
  LoginScreen,
  SignupScreen,
  ResetPasswordScreen,
  HomeScreen,
  ProfileScreen,
  DrawerContent,
  AuthLoadingScreen
} from './src/screens';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import firebase from 'firebase/app'
import {firebaseConfig} from './src/core/config'


if(!firebase.apps.length){
  firebase.initializeApp(firebaseConfig)
}

const Stack = createNativeStackNavigator();
const Tab=createBottomTabNavigator();
const Drawer=createDrawerNavigator();

export default function App() {
  return (

    //<------------This is Todo_13---------->
    // <NavigationContainer>
    //   <Stack.Navigator
    //     // initialRouteName='Screen2'
    //     screenOptions={{headerShown:true}}
    //   >
    //     <Stack.Screen name='Screen1' component={Screen1}/>
    //     <Stack.Screen name='Screen2' component={Screen2}/>
    //     <Stack.Screen name='Screen3' component={Screen3}/>
    //   </Stack.Navigator>
    // </NavigationContainer>


    //<---------------This is Todo_14------------------------>
    <Provider theme={theme}>
      <NavigationContainer>
        <Stack.Navigator 
          initialRouteName='AuthLoadingScreen'
          screenOptions={{headerShown:false}}
        >
          <Stack.Screen name='AuthLoadingScreen' component={AuthLoadingScreen} />
          <Stack.Screen name='StartScreen' component={StartScreen}/>
          <Stack.Screen name='LoginScreen' component={LoginScreen}/>
          <Stack.Screen name='SignupScreen' component={SignupScreen}/>
          <Stack.Screen name='ResetPasswordScreen' component={ResetPasswordScreen} />
          <Stack.Screen name='HomeScreen' component={DrawerNavigation} />
        </Stack.Navigator>

      </NavigationContainer>
    </Provider>
  );
}

function DrawerNavigation(){
  return(
    <Drawer.Navigator drawerContent={DrawerContent}>
      <Drawer.Screen name='BottomNavigation' component={BottomNavigation} />
    </Drawer.Navigator>
  )
}


function BottomNavigation(){
  return(
    <Tab.Navigator>
      <Tab.Screen name='Home' component={HomeScreen} 
        options={{
          headerShown:false,
          tabBarIcon:({size})=>{
            return(
              <Image
                style={{width:size,height:size}}
                source={
                  require('./assets/homeicon.png')
                }
              />
            )
          }
        }}
      />
      <Tab.Screen name="Profile" component={ProfileScreen}
        options={{
          headerShown:false,
          tabBarIcon:({size})=>{
            return(
              <Image
                style={{width:size,height:size}}
                source={
                  require('./assets/profile.png')
                }
              />
            )
          }
        }}
      />
    </Tab.Navigator>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
