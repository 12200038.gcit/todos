import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Header from '../component/Header'
import Paragraph from '../component/Paragraph'
import Button from '../component/Button'
import Background from '../component/Background'
import Logo from '../component/Logo'
const StartScreen = ({navigation}) => {
  return (
    <Background>
      <Logo/>
      <Header>Login template</Header>
      <Paragraph>The easiest way to start with your amazing application.</Paragraph>
      <Button mode='outlined' onPress={()=>navigation.navigate("LoginScreen")}>Login</Button>
      <Button mode='contained' onPress={()=>navigation.navigate("SignupScreen")}>Sign up</Button>
    </Background>
  )
}

export default StartScreen
