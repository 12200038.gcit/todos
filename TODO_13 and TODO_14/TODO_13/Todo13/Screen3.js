import { StyleSheet, Text, View, Button } from 'react-native'
import React from 'react'

const Screen3 = ({navigation}) => {
  return (
    <View>
      <Text>Screen3</Text>
      <Button title='replace screen1' onPress={()=>navigation.replace('Screen1')} />
    </View>
  )
}

export default Screen3

const styles = StyleSheet.create({})