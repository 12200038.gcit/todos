import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
        <View style={styles.sub1}><Text>1</Text></View>
        <View style={styles.sub2}><Text>2</Text></View>
        <View style={styles.sub3}><Text>3</Text></View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection:'row',
    padding:50,
    paddingRight:100,
    backgroundColor: '#fff',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  sub1:{
    backgroundColor:'red',
    height:250,
    flex:0.4,
    justifyContent:'center',
    alignItems:'center',
  },
  sub2:{
    backgroundColor:'#9852F9',
    flex:1,
    height:250,
    justifyContent:'center',
    alignItems:'center',

  },
  sub3:{
    backgroundColor:'green',
    height:250,
    flex:0.05,
    justifyContent:'center',
    alignItems:'center',
  }
});
