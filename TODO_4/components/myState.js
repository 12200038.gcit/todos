import React, { Component } from 'react';
import { View, Text, Button , TextInput} from 'react-native';
class MyState extends Component{
    state={message:""}
    onPress=()=>{
        this.setState({
            message: "Thank you for pressing me"
        })
        
    }
    render(){
        return(
            <View>
                <Text>{this.state.message}</Text>
               <Button
               onPress={this.onPress}
               title="Press me"
               />
            </View>
        )
    }
}
export default MyState;