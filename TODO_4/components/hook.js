import React, {useState} from 'react'
import { View,Text, Button } from 'react-native'

function hook() {
    const [count, setCount]=useState(0)
  return (
    <View>
        <Text>{count} hooks</Text>
        <Button
            title="Press me"
            onPress={()=>setCount(count+1)}
        />
    </View>
  );
}
export default hook;