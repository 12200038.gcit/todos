import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import MyState from './components/myState';
import Hook from './components/hook';
import Properties from './components/properties';
export default function App() {
  return (
    <View style={styles.container}>
      <StatusBar style='auto'/>
      <MyState/>
      <Hook/>
      <Properties name="Bir Bdr Tamang"/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
