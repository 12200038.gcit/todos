import { StyleSheet, Text, View,Button,Image,Dimensions } from 'react-native'
import React from 'react'
import Title from '../components/ui/Title'
import Colors from '../constants/Colors'
import PrimaryButton from '../components/ui/PrimaryButton'
const GameOverScreen = ({roundsNumber,userNumber,onStartNewGame}) => {

    const {width,height}=useWindowDimensions()

  return (
    <View style={styles.container}>
     <Title style={styles.text}>Game Over!</Title>
     <View style={styles.imageContainer}>
        <Image style={styles.image} source={require('../assets/success.jpg')} />
     </View>
     <Text style={styles.summaryText}>Your phone needed <Text style={styles.highlight}>{roundsNumber}</Text> rounds to guess the number <Text style={styles.highlight}>{userNumber}</Text></Text>
     <PrimaryButton onPress={onStartNewGame}>Start New Game</PrimaryButton>
    </View>
  )

  }
export default GameOverScreen

const deviceWidth=Dimensions.get('window').width
const styles = StyleSheet.create({
    container:{
        height:'100%',
        // justifyContent:'center',
        paddingTop:100,
        alignItems:'center'
    },
    text:{
        color:'white',
        fontSize:20,
        // width:'100%'
    },
   imageContainer:{
       width: deviceWidth <380 ? 150:300,
       height:deviceWidth <380 ? 150:300,
       borderRadius:150,
       borderWidth:3,
       borderColor:Colors.primary800,
       overflow:'hidden',
       margin:36
   },
   summaryText:{
       fontSize:20,
       textAlign:'center',
       color:'white',
       fontFamily:'open-sans',
   },
   image:{
       width:'100%',
       height:'100%'
   },
   highlight:{
        color:'red',
        fontFamily:'open-sans',
   }
})