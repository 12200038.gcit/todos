import { StyleSheet, Text, View,Pressable } from 'react-native'
import React from 'react'

const PrimaryButton = ({children,onPress}) => {
  return (
    <View style={styles.buttonOuterContainer}>
        <Pressable onPress={onPress} android_ripple={{color:'#64023'}} style={({pressed})=>pressed ? [styles.buttonInnerContainer,styles.pressed]:styles.buttonInnerContainer}>
            <Text style={styles.buttonText}>{children}</Text>
        </Pressable>
    </View>
    
  )
}

export default PrimaryButton

const styles = StyleSheet.create({
    buttonInnerContainer:{
        backgroundColor:'#72063c',
        borderRadius:28,
        // borderWidth:1,
        paddingVertical:8,
        paddingHorizontal:16,
        elevation:2,
        margin:4
    },
    buttonOuterContainer:{
        borderRadius:28,
        margin:4,
        overflow:'hidden'

    },
    buttonText:{
        color:'white',
        textAlign:'center'
    },
    pressed:{
        opacity:0.75
    }
})