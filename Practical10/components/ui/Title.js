import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Colors from '../../constants/Colors'

const Title = ({children,style}) => {
  return (
   <Text style={[styles.title,style]}>{children}</Text>
  )
}

export default Title

const styles = StyleSheet.create({
    title:{
      fontFamily:'open-sans-bold',
      fontSize:24,
      color:'#fff',
      borderWidth:2,
      textAlign:'center',
      padding:12,
      fontWeight:'bold',
      borderColor:'white',
      maxWidth:'80%',
      width:300
    }
})